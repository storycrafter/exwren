defmodule Exwren do
  @on_load :init
  @moduledoc """
    Documentation for Exwren
  """

  def init do
    :ok = :erlang.load_nif('./priv/exwren', 0)
  end

  @doc """
    Test stub function which will disappear soon
  """
  def sum(_, _) do
    exit(:nif_library_not_loaded)
  end
end