cmake_minimum_required(VERSION 3.16)
set(PROJECT_NAME "exwren")
project(${PROJECT_NAME})

# Configuration types
SET(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "Configs" FORCE)
IF (DEFINED CMAKE_BUILD_TYPE AND CMAKE_VERSION VERSION_GREATER "2.8")
    SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS ${CMAKE_CONFIGURATION_TYPES})
ENDIF ()

set(CMAKE_CXX_STANDARD 14)

add_library(exwren MODULE src/exwren.c)

find_package(Git QUIET)
if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.gitmodules")
    # Update submodules as needed
    option(GIT_SUBMODULE "Check submodules during build" ON)
    if(GIT_SUBMODULE)
        message(STATUS "Submodule update")
        execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                RESULT_VARIABLE GIT_SUBMOD_RESULT
                COMMAND_ECHO STDERR)
        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
            message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
        endif()
    endif()
endif()

execute_process(COMMAND "erl" -eval 'io:format ("~s" , [lists:concat ([code:root_dir () , "/erts-" , erlang:system_info (version) , "/include"])])' -s init stop -noshell
        OUTPUT_VARIABLE EINCLUDE_DIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
        COMMAND_ECHO STDERR)

include_directories(${EINCLUDE_DIR})

SET(${PROJ_NAME}_PATH_INSTALL "priv" CACHE PATH "This directory contains installation Path")

# Install
#---------------------------------------------------#
INSTALL(TARGETS ${PROJ_NAME}
        DESTINATION "${${PROJ_NAME}_PATH_INSTALL}/lib/${CMAKE_BUILD_TYPE}/ ")