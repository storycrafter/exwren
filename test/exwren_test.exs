defmodule ExwrenTest do
  @moduledoc false
  use ExUnit.Case
  doctest Exwren

  test "Test sum" do
    assert Exwren.sum(1, 2) == 3
  end
end
